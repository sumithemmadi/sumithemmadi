# Welcome to My GitHub Profile!
[![Profile views count](https://komarev.com/ghpvc/?username=sumithemmadi)](https://github.com/sumithemmadi)
[![GitHub Sponsors](https://img.shields.io/github/sponsors/sumithemmadi)](https://github.com/sponsors/sumithemmadi)


[![Typing SVG](https://readme-typing-svg.herokuapp.com?font=Cursive&color=%2336BCF7&size=35&center=true&vCenter=true&lines=Hello+I'm+Sumith;I'm+a+student;I'm+a+developer)](https://github.com/sumithemmadi/)

Hello there! 👋 I'm [Sumith Emmadi](https://sumithemmadi.github.io), a passionate computer science engineering student currently pursuing my studies in the CSE branch at IIIT Surat. With a strong passion for coding and a background in computer science engineering, I have developed a solid foundation in software development. My dedication and enthusiasm drive me to constantly expand my skill set and stay up-to-date with the latest industry trends.

## ⭐ About Me

```zsh
> neofetch
```

<img align="left" src="https://raw.githubusercontent.com/sumithemmadi/sumithemmadi/main/rounded-image.png" width="150px" height="220px"/> 

```yaml
My Profile
-----------------------------------------------------------
Name      : Emmadi Sumith Kumar.
WhoamI    : Student 🏫.
Gender    : Male.
Location  : Telangana, India.
Hobbies   : Gaming, Coding , listening music 🎶.
Languages : Bash,C Lang, C++, Python, NodeJS , Typescript.
Learning  : Java, Kotlin.
Telegram  : @sumithemmadi.
```


I'm a versatile full stack developer with hands-on experience in various technologies. From crafting responsive front-end interfaces to building robust back-end systems, I enjoy the process of transforming ideas into functional applications. I also have a keen interest in machine learning, which allows me to apply data-driven approaches to solve complex problems and gain valuable insights from data.

## 👨‍🎓 Education

- Bachelor of Technology (B.Tech) in Computer Science and Engineering
  - [Indian Institute of Information Technology (IIIT), Surat](https://www.google.com/search?q=IIIT+Surat&oq=IIIT+Surat&aqs=chrome..69i57j69i60l3j69i65.7945j0j7&client=ms-android-xiaomi-rev1&sourceid=chrome-mobile&ie=UTF-8)
  - Expected Graduation: July, 2024


## 🧑‍💻 Skills

- **Languages**: C and C++ , JavaScript, TypeScript, Python
- **Frontend**: React.js, Next.js, HTML, CSS
- **Backend**: Node.js, Express.js, Python Flask
- **Databases**: MongoDB, MySQL
- **Machine Learning**: Scikit-learn, Pandas, NumPy
- **Version Control**: Git
- **Tools**: VS Code

<img src="https://skillicons.dev/icons?i=c,cpp,py,nodejs,typescript,js,css,html,bash,linux,git,androidstudio,java,githubactions&perline=7" alt="My Skills"/>

[![sumithemmadi's github programming languages](https://github-readme-stats-eight-theta.vercel.app/api/top-langs/?username=sumithemmadi&langs_count=10&layout=compact&theme=material-palenight&hide_border=true&bg_color=1F222E&title_color=F85D7F&icon_color=F8D866)](https://github.com/sumithemmadi/)
  
## 🚀 Projects

Throughout my journey, I have actively worked on various projects and contributed to the developer community. Here are some of my notable projects:

### 1. [truecallerjs](https://github.com/sumithemmadi/truecallerjs)

  - **Description**:  truecallerjs is a lightweight npm package that allows users to perform reverse phone number lookups using the Truecaller API. It simplifies the process of querying and handling Truecaller data, providing a seamless experience for developers.

  - **Technologies**: Typescript, Axios
    
<a href="https://github.com/sumithemmadi/truecallerjs">
    <img src="https://opengraph.githubassets.com/83b8abc33a7b26898a4846558d1c70d88bb7c08a9ac0493e8eccf7cd2e683d47/sumithemmadi/truecallerjs" alt="TruecallerJS" width="400" >
</a>



### 2. [truecallerpy](https://github.com/sumithemmadi/truecallerjs)

  - **Description**: TruecallerPy is a Python package that provides functionalities to interact with the Truecaller API.

  - **Technologies**: Python, JSON

<a href="https://github.com/sumithemmadi/truecallerpy">
    <img src="https://opengraph.githubassets.com/83b8abc33a7b26898a4846558d1c70d88bb7c08a9ac0493e8eccf7cd2e683d47/sumithemmadi/truecallerpy" alt="Truecallerpy" width="400" >
</a>

### 3. [json-to-plain-text](https://github.com/sumithemmadi/json-to-plain-text)

  - **Description**:  json-to-plain-text is a npm module that converts JSON-like data or plain JavaScript objects to a formatted plain text representation.

  - **Technologies**: Node.js
    
<a href="https://github.com/sumithemmadi/json-to-plain-text">
    <img src="https://opengraph.githubassets.com/83b8abc33a7b26898a4846558d1c70d88bb7c08a9ac0493e8eccf7cd2e683d47/sumithemmadi/json-to-plain-text" alt="json-to-plain-text" width="400" >
</a>


### 4. [bhimupijs](https://github.com/sumithemmadi/bhimupijs)

  - **Description**:  BHIMUPIJS is a npm module which can validate, verify and generate QR Codes for UPI IDs.

  - **Technologies**: Node.js

<a href="https://github.com/sumithemmadi/bhimupijs">
    <img src="https://opengraph.githubassets.com/83b8abc33a7b26898a4846558d1c70d88bb7c08a9ac0493e8eccf7cd2e683d47/sumithemmadi/bhimupijs" alt="bhimupijs" width="400" >
</a>


<ins>[and more](https://github.com/sumithemmadi?tab=repositories)</ind>

Feel free to explore my other repositories for more projects, including experiments, utilities, and prototypes.

## 🤝 Open Source Contributions

Apart from the projects mentioned above, I'm also trying to contribute to open source projects.

<!--
🛠
## Hackathons and Competitions

- [Hackathon/Competition 1]: Description, [Year]
- [Hackathon/Competition 2]: Description, [Year]

## 😜 Fun Facts

- Add a fun fact about yourself!

## ❣️ Interests

Apart from coding, I love hiking, playing guitar, and trying out new recipes.


## 👑 Achievements

- [Award/Achievement 1]: Description
- [Award/Achievement 2]: Description

-->

## ☎️ Get in Touch

I'm always eager to connect with fellow developers, tech enthusiasts, and collaborators. Feel free to reach out to me through the following channels:

- [![Gmail](https://img.shields.io/badge/Gmail-D14836?style=for-the-badge&logo=gmail&logoColor=white)](mailto:sumithemmadi244@gmail.com)
- [![Protonmail](https://img.shields.io/badge/ProtonMail-8B89CC?style=for-the-badge&logo=protonmail&logoColor=white)](mailto:sumithemmadi@protonmail.com)
- [![LinkedIn](https://img.shields.io/badge/sumithemmadi-%230077B5.svg?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/sumithemmadi)
- [![Twitter](https://img.shields.io/badge/sumithemmadi-%231DA1F2.svg?style=for-the-badge&logo=Twitter&logoColor=white)](https://www.twitter.com/sumithemmadi)
- [![Telegram](https://img.shields.io/badge/sumithemmadi-2CA5E0?style=for-the-badge&logo=telegram&logoColor=white)](https://t.me/sumithemmadi)
- [![Facebook](https://img.shields.io/badge/sumithemmadi-%231877F2.svg?style=for-the-badge&logo=Facebook&logoColor=white)](https://www.facebook.com/sumithemmadi)
- [![Instagram](https://img.shields.io/badge/sumithemmadi-%23E4405F.svg?style=for-the-badge&logo=Instagram&logoColor=white)](https://www.instagram.com/sumithemmadi)
- [sumithemmadi.github.io](https://sumithemmadi.github.io)

Let's connect and collaborate on exciting projects! Thank you for visiting my profile. Happy coding! 🚀

## 💝 Sponsor

If you find my projects helpful or inspiring, consider supporting me through GitHub Sponsors. Your sponsorship helps me dedicate more time and effort to open source development and creating impactful projects.

- **Sponsor Me**: [https://github.com/sponsors/sumithemmadi](https://github.com/sponsors/sumithemmadi/)
- **Paypal**: [paypal.me/sumithemmadi](https://paypal.me/sumithemmadi)
- **UPI ID** : sumithemmadi@paytm

### 💖 Sponsors

[![Sponsors](https://sumithemmadi.github.io/sponsor.svg)](https://github.com/sponsors/sumithemmadi/)

## ✨ Code Stats

  <p align = "center">
    <a href="https://github.com/sumithemmadi"><img alt="sumithemmadi's Github Stats" src="https://github-readme-stats.vercel.app/api/?username=sumithemmadi&show_icons=true&include_all_commits=true&count_private=true&theme=material-palenight&hide_border=true&bg_color=1F222E&title_color=F85D7F&icon_color=F8D866&line_height=28&rank_icon=github" height="192px"/></a>
    <a href="https://github.com/sumithemmadi"><img alt="sumithemmadi's Top Languages" src="https://denvercoder1-github-readme-stats.vercel.app/api/top-langs/?username=sumithemmadi&langs_count=10&layout=compact&theme=material-palenight&hide_border=true&bg_color=1F222E&title_color=F85D7F&icon_color=F8D866" height="192px"/></a>
    <a href="https://github.com/sumithemmadi"><img alt="sumithemmadi's Contribution Graph" src="https://github-readme-activity-graph.vercel.app/graph?username=sumithemmadi&theme=dracula&bg_color=1F222E&title_color=F85D7F&point=F8D866&line=F85D7F&color=a6accd&hide_border=true&radius=4.5" /></a>
  </p>
  
<!--
  [![sumithemmadi's github stats](https://github-readme-stats.vercel.app/api?username=sumithemmadi&show_icons=true&theme=tokyonight)](https://github.com/sumithemmadi/) <br>
  [![GitHub Streak](https://github-readme-streak-stats.herokuapp.com?user=sumithemmadi&theme=algolia&hide_border=true)](https://github.com/sumithemmadi/) <br>
  [![Sumith Emmadi](https://github-profile-trophy.vercel.app/?username=sumithemmadi&row=1)](https://github.com/sumithemmadi) <br>
-->

If you find my work helpful or inspiring, consider supporting me by giving a ⭐️ to the repositories you like and [sponsor me](https://github.com/sponsors/sumithemmadi/). It means a lot and keeps me motivated to do more.

Thank you for visiting my profile. Let's connect and collaborate! Happy coding! 🚀
